alter procedure sp_carga_tablas
as
begin
  set NOCOUNT on
  /* Variables del contenido del catalogo*/
  declare @cat_table_name varchar(255)
  /* Cursor carga información de columnas del catalogo*/
  declare tablas_cursor dynamic scroll cursor for select TABLE_NAME="name"
      from sysobjects where "type"='U' order by 1 asc
  open tablas_cursor
  fetch next tablas_cursor into @cat_table_name
  while(sqlstate='00000')
    begin
      /* -------------------------------------------------------------------------*/
      /* Para cada tabla del catalogo*/
      execute sp_agrega_nh_tabla(@cat_table_name)
      fetch next tablas_cursor into @cat_table_name
    end
  close tablas_cursor
  return 1
  set NOCOUNT off
end