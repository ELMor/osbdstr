alter procedure sp_agrega_nh_columnas(@p_tabla varchar(255)) as
begin
  set NOCOUNT on
  /* Variables del contenido del catalogo*/
  declare @cat_table_name varchar(255),@cat_columns_name varchar(255),
  @cat_columna_tipo
  /* Cursor carga información de columnas del catalogo*/
  declare columns_cursor dynamic scroll cursor for select TABLE_NAME=o."name",
      COLUMN_NAME=lower(c.cname),
      tipo=coltype
      from sysobjects as o,syscolumns as c
      where o."name"=@p_tabla and c.tname=o."name" order by
      1 asc
  open columns_cursor

/*Tipo de datos de columnas existentes*/
/*smallint, integer, char, long varchar, binary, varchar, time, numeric, timestamp, date*/
/*Tipo de datos de columnas que pueden llegar*/
/*smallint, integer, char, varchar, time, numeric, date*/
select @contador_columnas=0
  fetch next columns_cursor into @cat_table_name,@cat_columns_name,@cat_columna_tipo
  while(sqlstate='00000')
    begin
      select @contador_columnas+=1
      /* ------------------------------------------------------------*/
      /* Genero el array con los nombres de las columnas de la tabla */
      /* Array columnas[]                                            */
      /*-------------------------------------------------------------*/
      /* ----------------------------------------------------------------*/
      /* Genero el string para la select de todos los campos de la tabla */
	/* del tipo "Select Campo1, Campo2,...,CampoN from "+@p_tabla      */
	/* Almaceno la sentencia en @sentencia1_generada                    */
      /*-----------------------------------------------------------------*/
      fetch next columns_cursor into @cat_table_name,@cat_columns_name,@cat_columna_tipo
    end
  close columns_cursor
/*------------------------------*/
/* Ejecuto la sentencia generada*/
/*------------------------------*/
  declare tuplas_cursor dynamic scroll cursor for @sentencia1_generada
  @contador_tuplas=0
  open tuplas_cursor
  fetch next tuplas_cursor (Estructura de lectura)
  while(sqlstate='00000')
    begin
      /* ------------------------------------------------------------*/
      /* Para cada fila de la tabla @p_tabla                         */
      /*-------------------------------------------------------------*/
	@contador_tuplas+=1
	for i =1 to @contador_columnas
	      /* ----------------------------------------------------------------*/
	      /* Hago un insert en la tabla nh_datos con los siguientes valores  */
		/* hn_datos.nombre_tabla	=@cat_table_name  */
		/* hn_datos.nombre_columna	=Array_columnas[i]  */
		/* hn_datos.datos_tupla		=@contador_tuplas  */
		/* hn_datos.datos_valor		= string( valor columna[i] de Estructura lectura)*/
	      /*-----------------------------------------------------------------*/
	next
      fetch next tuplas_cursor (Estructura de lectura)
    end
  close tuplas_cursor
  return 1
  set NOCOUNT off
end