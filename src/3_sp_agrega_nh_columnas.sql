alter procedure sp_agrega_nh_columnas(@p_tabla varchar(255)) as
begin
  set NOCOUNT on
  /* Variables del contenido del catalogo*/
  declare @cat_table_name varchar(255),@cat_columns_name varchar(255),
  @cat_nombre_columna_inc smallint,@cat_columna_activa smallint,
  @cat_nulo char(1),@cat_pk char(1),@cat_def varchar(255),
  @cat_lon integer,@cat_prec integer
  /* Cursor carga información de columnas del catalogo*/
  declare columns_cursor dynamic scroll cursor for select TABLE_NAME=o."name",
      COLUMN_NAME=lower(c.cname),
      nulo=nulls,
      pk=in_primary_key,
      por_defecto=default_value,
      longitud=length,
      presicion=syslength
      from sysobjects as o,syscolumns as c
      where o."name"=@p_tabla and c.tname=o."name" order by
      1 asc
  open columns_cursor
  fetch next columns_cursor into @cat_table_name,@cat_columns_name,
    @cat_nulo,@cat_pk,@cat_def,@cat_lon,
    @cat_prec
  while(sqlstate='00000')
    begin
      /* -------------------------------------------------------------------------*/
      /* Para cada columna del catalogo se actualiza la información de NH_COLUMNAS*/
      select @cat_nombre_columna_inc=0
      select @cat_columna_activa=1
      if @cat_def='autoincrement' select @cat_nombre_columna_inc=1
      update nh_columnas set
        nombre_columna_inc=@cat_nombre_columna_inc,
        columna_nulo=@cat_nulo,
        columna_pk=@cat_pk,
        columna_longitud=@cat_lon,
        columna_prec=@cat_prec
        where @cat_table_name=nombre_tabla and @cat_columns_name=nombre_columna
      if sqlstate<>'00000' select 'Error al modificar - Tabla:'+@cat_table_name+' Columna:'+@cat_columns_name
      if @@rowcount=0
        /*Carga de nuevo registro*/
        begin
          insert into nh_columnas(nombre_tabla,
            nombre_columna,nombre_columna_inc,
            columna_activa,columna_nulo,columna_pk,columna_longitud,columna_prec) values(
            @cat_table_name,@cat_columns_name,@cat_nombre_columna_inc,
            @cat_columna_activa,@cat_nulo,@cat_pk,@cat_lon,@cat_prec)
          if sqlstate<>'00000' select 'Error al insertar - Tabla:'+@cat_table_name+' Columna:'+@cat_columns_name
        end
      /* Para cada columna FIN*/
      /*-------------------*/
      fetch next columns_cursor into @cat_table_name,@cat_columns_name,
        @cat_nulo,@cat_pk,@cat_def,@cat_lon,
        @cat_prec end
  close columns_cursor
  return 1
  set NOCOUNT off
end