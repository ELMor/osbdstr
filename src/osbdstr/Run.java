package osbdstr;

import java.sql.Connection;
import java.util.Properties;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DatabaseMetaData;
import java.util.Vector;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class Run {
  private Connection conn=null;
  private DatabaseMetaData dbmd=null;
  private String username;

  public Run(String dbtype, String surl, String login, String pass, int procID)
		throws Exception {

    username=login.toUpperCase();
		Properties props = new Properties();
		props.put("user"    , login);
		props.put("password", pass);
		if( dbtype.equalsIgnoreCase("oracle") ){
			//Orcacle 9i
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			surl="jdbc:oracle:thin:@"+surl;
		}else if( dbtype.equalsIgnoreCase("sqlserver") ){
			surl="jdbc:weblogic:mssqlserver4:"+surl;
			Class.forName("weblogic.jdbc.mssqlserver4.Driver").newInstance();
		}else if( dbtype.equalsIgnoreCase("sybase") ){
			//Sybase ASE, ASA
			surl="jdbc:sybase:Tds:"+surl;
			props.put("USE_METADATA","false");
			Class.forName("com.sybase.jdbc2.jdbc.SybDriver").newInstance();
		}else if( dbtype.equalsIgnoreCase("informix") ){
			//Informix Dynamic Server
			surl="jdbc:weblogic:informix4:"+surl;
			Class.forName("weblogic.jdbc.informix4.Driver").newInstance();
		}else{
			System.out.println("BD:"+dbtype+" No encontrada");
			throw new Exception(dbtype+" Not found");
		}

		conn=DriverManager.getConnection(surl,props);
    dbmd=conn.getMetaData();

    cargaTablas(procID);

	}

  private static void Instrucciones()
  {
    System.out.println("java osdbstr.Run tipoBD BDURL login password procID");
		System.out.println("\ttipoBD  : [ oracle | sqlserver | sybase | informix ]");
		System.out.println("\tBDURL   : (oracle)    host:port:sid");
		System.out.println("\t          (sqlserver) host:port");
		System.out.println("\t          (sybase)    host:port");
		System.out.println("\t          (informix)  database@host:port");
		System.out.println("\tlogin   : Usuario de BBDD");
		System.out.println("\tpassword: Password del usuario de BBDD");
		System.out.println("\tprocID  : 1 cargar datos en nh_tablas y nh_columnas ");
		System.out.println("\t        : 2 cargar datos en nh_datos ");
  }

  public static void main(String[] args)
  {
    if( args.length!=5 )
    {
      Instrucciones();
      return;
    }
    try {
      int pid=0;
      try{
        pid=Integer.parseInt(args[4]);
      }catch(Exception eParse){
        eParse.printStackTrace();
        System.exit(-1);
      }
      new Run(args[0],args[1],args[2],args[3],pid);
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public void cargaTablas(int pid)
  throws Exception
  {
    String tiposT[]=new String[] {"TABLE"};
    //Tablas del usuario username en el esquema username
    ResultSet rs=dbmd.getTables(null,null,null,tiposT);

    while( rs.next() )
    {
      String tableName=rs.getString(3);
      if( pid==1 )
        agregaTabla( tableName );
      if( pid==2 && checkActiva(tableName) )
        cargaDatos(tableName);
    }
  }

  public void agregaTabla(String tableName)
  throws Exception
  {
    System.out.println("Agregando tabla "+tableName);

    Statement st=conn.createStatement();
    if( !tableName.equalsIgnoreCase("nh_tablas") &&
        !tableName.equalsIgnoreCase("nh_columnas") &&
        !tableName.equalsIgnoreCase("nh_datos")       )
    {
      ResultSet rs=st.executeQuery
        (
          "select count(*) from nh_tablas where nombre_tabla='"+
          tableName.toLowerCase()+"'"
        );
      rs.next();
      if( rs.getInt(1)==0 )
        st.executeUpdate
          (
             "insert into nh_tablas(nombre_tabla, tabla_activa) "+
             "values('"+tableName.toLowerCase()+"',1) "
          );

      agregaColumnas(tableName);
    }
  }

  public void agregaColumnas(String tableName)
  throws Exception
  {
    System.out.println("Agregando columnas para "+tableName);
    Statement st=conn.createStatement();
    ResultSet rs=dbmd.getColumns(null,null,tableName,null);
    while( rs.next() )
    {
      String colName=rs.getString(4);
      int    colNull=rs.getInt(11);
      String colDefa=(rs.getString(13)==null ? "0" : rs.getString(13));
      int    colLong=rs.getInt(7);
      int    colPrec=rs.getInt(9);
      //Se intenta insertar la columna, si est� duplicada, salta el catch
      try{
        st.executeUpdate
          (
            "insert into nh_columnas"+
            "(nombre_tabla,nombre_columna,columna_activa) "+
            "values('"+tableName.toLowerCase()+"',"+
                    "'"+colName.toLowerCase()+"',"+
                    "1"+
                    ")"
          );
      }catch(Exception e){
      }
      //Update (la tupla est� seguro por la sentencia anterior
      st.executeUpdate
        (
          "Update nh_columnas set "+
          "  nombre_columna_inc= "+ (colDefa.equalsIgnoreCase("autoincrement") ? "1" : "0")+
          "  ,columna_nulo="+colNull+
          "  ,columna_longitud="+colLong+
          "  ,columna_prec="+colPrec+
          " where nombre_tabla='"+
            tableName.toLowerCase()+"' and nombre_columna='"+colName.toLowerCase()+"'"
        );
    }
    //La info sobre PK se obtiene aparte
    rs=dbmd.getPrimaryKeys(null,null,tableName);
    while( rs.next() )
    {
      String colName=rs.getString(4);
      short  colSequ=rs.getShort(5);
      st.executeUpdate
        (
          "Update nh_columnas set "+
          "  columna_pk="+colSequ+
          " where nombre_tabla='"
            +tableName.toLowerCase()+"' and nombre_columna='"
            +colName.toLowerCase()+"'"
        );
    }
  }

  public void cargaDatos(String tableName)
  throws Exception
  {
    Statement stColumnas=conn.createStatement();
    ResultSet rsColumnas=stColumnas.executeQuery
      (
        "select nombre_columna from nh_columnas "+
        "where nombre_tabla='"+tableName.toLowerCase()+"' "+
        "order by nombre_columna"
      );
    String sqlSelect="";
    Vector vCols=new Vector();
    while(rsColumnas.next())
    {
      vCols.add(rsColumnas.getString(1));
      sqlSelect+=rsColumnas.getString(1)+",";
    }
    rsColumnas.close();
    stColumnas.close();

    if( sqlSelect.equals("") )
    {
      System.out.println("No hay columnas activas en "+tableName);
      return;
    }
    //Quitar ultimo PYC
    sqlSelect=sqlSelect.substring(0,sqlSelect.length()-1);

    //Select completa sobre la tabla
    sqlSelect="Select "+sqlSelect+" from "+tableName;
    Statement stTuplas=conn.createStatement();
    ResultSet rsTuplas=stTuplas.executeQuery(sqlSelect);
    //Inicio de inserciones
    int numTupla=0;
    while( rsTuplas.next() )
    {
      numTupla++;
      for(int i=0;i<vCols.size();i++)
      {
        Statement st=conn.createStatement();
        st.executeUpdate
        (
          "Delete from nh_datos where nombre_tabla='"+tableName.toLowerCase()+"'"+
          " and nombre_columna='"+((String)vCols.elementAt(i)).toLowerCase()+"'"+
          " and datos_tupla="+numTupla
        );
        st.executeUpdate
        (
          "Insert into nh_datos(nombre_tabla,nombre_columna,datos_tupla,datos_valor) "+
          "values ("+
          "'"+tableName.toLowerCase()+"',"+
          "'"+((String)vCols.elementAt(i)).toLowerCase()+"',"+
          numTupla+","+
          "'"+rsTuplas.getString(i+1)+"'"+
          ")"
        );
        st.close();
      }
    }
  }

  private boolean checkActiva(String tableName)
  throws Exception
  {
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery
      (
        "select tabla_activa from nh_tablas "+
        "where nombre_tabla='"+tableName.toLowerCase()+"'"
      );
    if( !rs.next() )
      return false;
    if( rs.getInt(1)==2 )
      return true;
    return false;
  }
}